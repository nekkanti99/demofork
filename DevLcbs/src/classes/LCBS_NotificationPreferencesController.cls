public with sharing class LCBS_NotificationPreferencesController {
Public User u {get;set;}
Public Contact c {get;set;}
public String PAGE_CSS { get; set; } 
//Public String returnURL{get;set;}

Public LCBS_NotificationPreferencesController (){
   // c = new contact();
 PAGE_CSS = LCBSParameters.PAGE_CSS_URL;

    u =  [select id,Contactid,AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
    
    c = [select id,High_Priority__c,Low_Priority__c,Medium_Priority__c,Email,phone from Contact where id=:u.Contactid];

  //  returnURL = ApexPages.currentPage().getParameters().get('retURL');
    
   
}

public PageReference alertInfoSave() {
    
    try{
       update c;
       }catch(DmlException e){}
       return null;
       
    }
    
public PageReference alertInfoCancel() {
       Pagereference pg = New Pagereference(LCBSParameters.HomePage_URL);
       pg.setRedirect(true);
       return pg; 
       
    }
    

}