/*
Name         : CustomLoginController
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : Honeywell-LCBS 
Created Date : 
Usages       : 
Modified By  :
*/
public class CustomLoginController {

public String username{get; set;}
public String password {get; set;}

public pageReference doLogin()
{  
    return Site.login(username,password,'/apex/LCBS_ContractorOwnerDashboard');
}
}