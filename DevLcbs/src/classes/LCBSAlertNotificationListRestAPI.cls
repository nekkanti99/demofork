@RestResource(urlMapping='/AlertNotificationList/*')
  
global class LCBSAlertNotificationListRestAPI {

         global class TransactionId{
            public String Id;
            
            public TransactionId(String Id) {
                this.Id= Id;
            }
        }
        
         global class NotificationType {
            public String NotificationType;
            
            public NotificationType(String NotificationType) {
                this.NotificationType = NotificationType;
            }
        }
        
        global class EmailStatus {
            public String Status;
            public String ErrorMessage;
            
            public EmailStatus(String Status, String ErrorMessage) {
                this.Status = Status;
                this.ErrorMessage = ErrorMessage;
            }
        }
        
        global class AlertNotificationResponse {
            public TransactionId TransactionId;
            public NotificationType NotificationType;
            public EmailStatus EmailStatus;
            
            public AlertNotificationResponse(String TId, String notifyType, String mailStat, String mailErr){
                TransactionId = new TransactionId(TId);
                NotificationType = new NotificationType(notifyType);
                EmailStatus = new EmailStatus(mailStat, mailErr);
            }
        }
    
    @HttpPost
    global static AlertNotificationResponse AlertNotification() {
        User users;
        LCBS_RequestResponse_Log__c req = new LCBS_RequestResponse_Log__c();
    
        RestRequest restreq = RestContext.request;
        Blob restbody = restreq.requestBody;
        String bodyString;
        String UserId;
        String Receiver;
        String Subject;
        String MessageBody;
        String CreatedDate;
        String AlertCode;
        String Priority;
        String BuildingId;
        String CloudAlertId;
        AlertNotificationResponse response;
        
        String NotificationType = '';
        String Status = '';
        String ErrMsg = '';
        String TransactionId='';
        Boolean Email = false;

        bodyString = restbody.toString();
        map<String, Object> reqbody = (map<String, Object>)JSON.deserializeUntyped(bodyString);
        List<Object> emailnotifications = (List<Object>)reqbody.get('Email');
        
        System.debug('Request Body:::'+reqbody);
        System.debug('Email Notifications in Request:::'+emailnotifications);
        
        Set<String> UserIdSet = new Set<String>();
        for(Object emailnote:emailnotifications) {
            map<String, Object> ens = (map<String, Object>)emailnote;                
            UserIdSet.add((String)ens.get('UserId'));
        }
        
        Map<String,User> UsersMap = new Map<String,User>([SELECT Id, Contact.High_Priority__c, Contact.Medium_Priority__c, Contact.Low_Priority__c, Contact.Email FROM User WHERE Id IN :UserIdSet]);
        System.debug('Users Map:::'+UsersMap);
        
        for (Object emailnote:emailnotifications) {
        
            map<String, Object> en = (map<String, Object>)emailnote;                
            UserId = (String)en.get('UserId');
            Subject = (String)en.get('Subject');
            MessageBody = (String)en.get('MessageBody');
            CreatedDate = (String)en.get('CreatedDate');
            AlertCode = (String)en.get('AlertCode');
            Priority = (String)en.get('Priority');
            BuildingId = (String)en.get('BuildingId');
            CloudAlertId = (String)en.get('CloudAlertId');
            
            users = UsersMap.get(UserId);
            if((Priority == 'High' && users.Contact.High_Priority__c =='Email')||(Priority == 'Medium' && users.Contact.Medium_Priority__c == 'Email')||(Priority == 'Low' && users.Contact.Low_Priority__c == 'Email'))
            {
                  try{
                        List<String> sendTo = new List<String>();
                        List<Messaging.SingleEmailMessage> sitemails = new List<Messaging.SingleEmailMessage>();          
                        Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
                        sendTo.add(users.Contact.Email);
                        sitemail.settargetObjectId(users.Contact.id);
                        sitemail.setToAddresses(sendTo);
                        sitemail.setSenderDisplayName('Building Notification');
                        sitemail.setSubject(Subject);
                        sitemail.setSaveAsActivity(true);            
                        string emailMessage = MessageBody;
                        sitemail.setHtmlBody(emailMessage);
                        sitemails.add(sitemail);
                        Messaging.sendEmail(sitemails); 
                        NotificationType = 'Email';
                        Status = 'Success';
                        ErrMsg = '';  
                } catch (Exception ex){
                    system.debug(ex.getMessage());
                    NotificationType = 'Email';
                    Status = 'Failed';
                    ErrMsg = ex.getMessage();       
                 } 
            } 
        
            req.Name = req.id;
            req.Request__c = bodyString;
            req.Response__c = NotificationType +'\n'+Status +'\n'+ErrMsg;
        }
        insert req;
        response = new AlertNotificationResponse(req.id,NotificationType,Status,ErrMsg);
        return response;
    }
}