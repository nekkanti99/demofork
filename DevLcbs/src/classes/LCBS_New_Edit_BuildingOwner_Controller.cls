/************************************
Name         : LCBSBuildingOwnerEditController
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/
public class LCBS_New_Edit_BuildingOwner_Controller {
public id buildingOwnerId{get;set;}
public id buildingId {get;set;}
public string pageMode{get;set;}
public Building_Owner__c BuldEdit{set;get;}
public User u {get;set;}
public Building__c building {get;set;}
public user currentuser {get;set;}
public transient List<Messaging.SingleEmailMessage> mails {get;set;}
 public LCBS_New_Edit_BuildingOwner_Controller()
 {
  BuldEdit = new Building_Owner__c();
  pageMode = apexpages.currentpage().getparameters().get('act');
  
  currentuser = [select id,name,contact.id from user where id=:userinfo.getuserid()];
  buildingOwnerId = apexpages.currentpage().getparameters().get('id');
  
  buildingId = apexpages.currentpage().getparameters().get('buildingid'); 
  if(buildingOwnerId != NULL)
  {
  BuldEdit =[Select id,Name,CreatedBy.id,Address__c,Address_1__c,Customer_Type__c,Address_2__c,City__c,Contractor_Branch_Location__c,Company_Company_Location12__r.name,Company_Company_Location12__c,Country__c,Email_Address__c,First_Name__c,Last_Name__c,Phone_Number__c,Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c where id =: buildingOwnerId]; 
  } 
 system.debug('Test@@@@@@'+buildingOwnerId );
 system.debug('Test@@@@@@'+pageMode );
 system.debug('Test@@@@@@'+buildingId );
 building = [select id,name,CreatedBy.id,Owner_Approved__c,Owner_Rejected__c,OwnerId,Building_Owner__c from Building__c where id=:buildingId];
 u = [select id,Account.id from user where id=:building.Createdby.id];
 }
 public PageReference buidingOwnerCancel() {
        PageReference p1 = new PageReference('/LCBS_MoreInfo_BuildInfo?id='+buildingId);
        p1.setredirect(true);
        return p1;
       
    }  
   public pagereference equipmentPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/equipments/'+u.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}

public pagereference alertsPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 // string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/alerts/'+u.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
public pagereference controlsPage()
{    
// String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/controls/'+u.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
} 
 public PageReference buidingOwnerUpdate() {   
    try{   
     // update BuldEdit;
        if(pageMode == 'e')
        {     
         update BuldEdit;    
        }
        else if(pageMode == 'n')
        {
         insert BuldEdit;
         building.Building_Owner__c = BuldEdit.id;
         update building;
         sendEmail();
       /*  EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate'];
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            Contact c = [select id, Email from Contact where email <> null order by name ASC limit 1];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
            sendTo.add(BuldEdit.Email_Address__c);
            system.debug('Email address@@@@@@@@@:'+BuldEdit.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(c.id);           
            mail.setSenderDisplayName('LCBS Building Approval');
           // mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
            //string emailMessage ='Your heating and cooling contractor is requesting data access to your building'+ ' '+build.Name+'.  To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            mail.whatid = BuldEdit.id;
            mails.add(mail);
            system.debug('mails ########:'+mails);
            Savepoint sp1 = Database.setSavepoint();
            system.debug('mails ^^^^^^^:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            Database.rollback(sp1);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
            } */
    }
    }
    catch(DmlException e)
    {  
     String error = e.getMessage(); 
     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error)); 
    }                     
     PageReference p = new PageReference('/LCBS_MoreInfo_BuildInfo?id='+buildingId);
        p.setredirect(true);
        return p;
 }  
 public void sendEmail(){
        EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate'];
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
           Contact c = [select id, Email from Contact where email <> null limit 1];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
            sendTo.add(BuldEdit.Email_Address__c);
            system.debug('Email address@@@@@@@@@:'+BuldEdit.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(currentuser.contactid);           
            mail.setSenderDisplayName('LCBS Building Approval');
            mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
           // string emailMessage ='Your heating and cooling contractor is requesting data access to your building To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            //mail.setsubject('LCBS Building Approval');
            mail.whatid = building.id;
            mails.add(mail);
           
            system.debug('mails ########:'+mails);
            Savepoint sp1 = Database.setSavepoint();
            system.debug('mails ^^^^^^^:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            Database.rollback(sp1);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
            
             
           }
       }     
}