public class LCBS_Distributor_Page1_Controller {

    public PageReference action3() {
        return null;
    }


    public PageReference action2() {
        return null;
    }

    set<id> accID = new set<id>();
    public List<Account> acc{get;set;}
    public LCBS_Distributor_Page1_Controller(){
        //RecordType RT = [select id,Name from RecordType where Name = 'ECC Americas ShipTo - Trade'];
        //acc = [select id,Name from Account where recordtypeid =: RT.Id];
        //acc=[Select RecordType.DeveloperName, RecordType.Name, RecordType.Id, RecordTypeId, Id, Name From Account where Recordtype.Name='ECC Americas ShipTo - Trade'];
        acc=[Select Id, Name, RecordTypeId From Account where RecordTypeId=:[Select Id from RecordType where Name='ECC Americas ShipTo - Trade'].Id];
        for(Account a : acc){
            accId.add(a.id);
        }
    }
    public List<Contact> getCon() {
        return [Select id,Name,FirstName,LastName,email,Phone,AccountId,account.name from Contact where AccountId in : AccID];
    }
    public PageReference action() {
        return null;
    }

}